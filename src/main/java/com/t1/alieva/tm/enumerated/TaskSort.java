package com.t1.alieva.tm.enumerated;

import com.t1.alieva.tm.comparator.CreatedComparator;
import com.t1.alieva.tm.comparator.NameComparator;
import com.t1.alieva.tm.comparator.StatusComparator;
import com.t1.alieva.tm.model.Task;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Comparator;

public enum TaskSort {

    BY_CREATED("Sort by created", CreatedComparator.INSTANCE::compare),
    BY_NAME("Sort by name", NameComparator.INSTANCE::compare),
    BY_STATUS("Sort by status", StatusComparator.INSTANCE::compare);

    @NotNull
    @Getter
    private final String displayName;

    @NotNull
    private final Comparator<Task> comparator;

    TaskSort(
            @NotNull final String displayName,
            @NotNull final Comparator<Task> comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

    @Nullable
    public static TaskSort toSort(@Nullable final String value) {
        if (value == null || value.isEmpty()) return null;
        for (@NotNull final TaskSort sort : values()) {
            if (sort.name().equals(value)) return sort;
        }
        return null;
    }

    @NotNull
    public Comparator<Task> getComparator() {
        return comparator;
    }

}
