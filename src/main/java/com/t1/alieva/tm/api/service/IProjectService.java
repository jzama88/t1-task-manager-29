package com.t1.alieva.tm.api.service;

import com.t1.alieva.tm.enumerated.Status;
import com.t1.alieva.tm.exception.AbstractException;
import com.t1.alieva.tm.exception.entity.AbstractEntityNotFoundException;
import com.t1.alieva.tm.exception.field.AbstractFieldException;
import com.t1.alieva.tm.model.Project;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface IProjectService extends IUserOwnedService<Project> {

    @NotNull
    Project create(
            @Nullable String userId,
            @Nullable String name) throws
            AbstractFieldException,
            AbstractEntityNotFoundException;

    @NotNull
    Project create(@NotNull String userId,
                   @Nullable String name,
                   @Nullable String description) throws
            AbstractFieldException,
            AbstractEntityNotFoundException;

    @NotNull
    Project updateById(@NotNull String userId,
                       @Nullable String id,
                       @Nullable String name,
                       @NotNull String description) throws
            AbstractFieldException,
            AbstractEntityNotFoundException;

    @NotNull
    Project updateByIndex(@NotNull String userId,
                          @Nullable Integer index,
                          @Nullable String name,
                          @NotNull String description) throws
            AbstractException;

    @NotNull
    Project changeProjectStatusByIndex(@NotNull String userId,
                                       @Nullable Integer index,
                                       @NotNull Status status) throws
            AbstractException;

    @NotNull
    Project changeProjectStatusById(@NotNull String userId,
                                    @Nullable String id,
                                    Status status) throws
            AbstractFieldException,
            AbstractEntityNotFoundException;
}
