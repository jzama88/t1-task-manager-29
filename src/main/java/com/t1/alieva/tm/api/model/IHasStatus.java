package com.t1.alieva.tm.api.model;

import com.t1.alieva.tm.enumerated.Status;
import org.jetbrains.annotations.NotNull;

public interface IHasStatus {

    @NotNull
    Status getStatus();

    void setStatus(@NotNull Status status);
}
