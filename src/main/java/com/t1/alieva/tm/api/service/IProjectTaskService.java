package com.t1.alieva.tm.api.service;

import com.t1.alieva.tm.exception.AbstractException;
import com.t1.alieva.tm.exception.entity.AbstractEntityNotFoundException;
import com.t1.alieva.tm.exception.field.AbstractFieldException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface IProjectTaskService {

    void bindTaskToProject(
            @NotNull String userId,
            @Nullable String projectId,
            @Nullable String taskId) throws
            AbstractFieldException,
            AbstractEntityNotFoundException;

    void removeProjectById(
            @NotNull String userId,
            @Nullable String projectId) throws
            AbstractException;

    void unbindTaskFromProject(
            @NotNull String userId,
            @Nullable String projectId,
            @Nullable String taskId) throws
            AbstractFieldException,
            AbstractEntityNotFoundException;
}
