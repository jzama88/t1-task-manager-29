package com.t1.alieva.tm.command.user;

import com.t1.alieva.tm.enumerated.Role;
import com.t1.alieva.tm.exception.AbstractException;
import com.t1.alieva.tm.exception.entity.AbstractEntityNotFoundException;
import com.t1.alieva.tm.exception.field.AbstractFieldException;
import com.t1.alieva.tm.exception.user.AbstractUserException;
import com.t1.alieva.tm.model.User;
import org.jetbrains.annotations.NotNull;

public final class UserViewProfileCommand extends AbstractUserCommand {

    @NotNull
    public static final String DESCRIPTION = "View profile of current user.";

    @NotNull
    public static final String NAME = "user-view-profile";

    @Override
    public void execute() throws
            AbstractException {
        @NotNull final User user = serviceLocator.getAuthService().getUser();
        System.out.println("[USER VIEW PROFILE]");
        System.out.println("ID:" + user.getId());
        System.out.println("LOGIN:" + user.getLogin());
        System.out.println("FIRST NAME:" + user.getFirstName());
        System.out.println("MIDDLE NAME:" + user.getMiddleName());
        System.out.println("LAST NAME:" + user.getLastName());
        System.out.println("E-MAIL:" + user.getEmail());
        System.out.println("ROLE:" + user.getRole().getDisplayName());
        showUser(user);
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return Role.values();
    }
}
